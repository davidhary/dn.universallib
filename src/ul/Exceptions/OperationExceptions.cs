using System;

namespace isr.io.UL
{
    /// <summary>Handles operation exceptions.</summary>
    /// <remarks>Use this class to handle exceptions that might be thrown
    /// exercising open, close, hardware access, and other similar operations. <para> 
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 07/29/03, 1.0.1305. Created </para></remarks>
    [Serializable()]
    public class OperationException : ExceptionBase
    {
        private const string _DefMessage = "Operation failed.";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructs a new exception using the internal default message.</summary>
        /// <remarks></remarks>
        public OperationException() : base( _DefMessage )
        {
        }

        /// <summary>Constructs a new exception using the given message.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <remarks></remarks>
        public OperationException( string message ) : base( message )
        {
        }

        /// <summary>Constructs a new exception using the given message and inner exception.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Specifies the exception that was trapped for 
        ///   throwing this exception.</param>
        /// <remarks></remarks>
        public OperationException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>   Protected constructor to deserialize data. </summary>
        /// <remarks>   David, 2022-01-18. </remarks>
        /// <param name="info">     Represents the SerializationInfo of the exception. </param>
        /// <param name="context">  Represents the context information of the exception. </param>
        protected OperationException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

        #region " METHODS "

        /// <summary>Overrides the GetObjectData method to serialize custom values.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        /// <remarks> David, 08/23/03, 1.0.1333. Inherit operation exception. </remarks>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            base.GetObjectData( info, context );
        }

        #endregion

    }

    /// <summary>Handles open operation exceptions.</summary>
    /// <remarks>Use this class to handle exceptions that might be thrown
    /// when attempting to open an operation such as a state machine. <para> 
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 07/29/03, 1.0.1305. Created </para><para>
    /// David, 08/23/03, 1.0.1333. Inherit operation exception </para></remarks>
    [Serializable()]
    public class OperationOpenException : OperationException
    {
        private const string _DefMessage = "Operation failed opening.";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>
        /// Initializes a new instance of this class.
        /// Uses the default message.
        /// </summary>

        public OperationOpenException() : base( _DefMessage )
        {
        }

        /// <summary>Constructs a new exception using the given message.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <remarks></remarks>
        public OperationOpenException( string message ) : base( message )
        {
        }

        /// <summary>Constructs a new exception using the given message and inner exception.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Specifies the exception that was trapped for 
        ///   throwing this exception.</param>
        /// <remarks></remarks>
        public OperationOpenException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected OperationOpenException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }

    /// <summary>   Handles close operation exceptions. </summary>
    /// <remarks>
    /// Use this class to handle exceptions that might be thrown when attempting to close an
    /// operation such as a state machine. <para>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para> <para>
    /// Licensed under The MIT License. </para> <para>
    /// David, 07/29/03, 1.0.1305: Created </para> <para>
    /// David, 08/23/03, 1.0.1333. Inherit operation exception</para>
    /// </remarks>
    [Serializable()]
    public class OperationCloseException : OperationException
    {
        private const string _DefMessage = "Operation failed closing.";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>
        /// Initializes a new instance of this class.
        /// Uses the default message.
        /// </summary>

        public OperationCloseException() : base( _DefMessage )
        {
        }

        /// <summary>Constructs a new exception using the given message.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <remarks></remarks>
        public OperationCloseException( string message ) : base( message )
        {
        }

        /// <summary>Constructs a new exception using the given message and inner exception.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Specifies the exception that was trapped for 
        ///   throwing this exception.</param>
        /// <remarks></remarks>
        public OperationCloseException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected OperationCloseException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }

    /// <summary>   Handles initialize operation exception. </summary>
    /// <remarks>
    /// Use this class to handle exceptions that might be thrown when attempting to initialize an
    /// operation. <para>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para> <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 07/29/03, 1.0.1305.Created </para><para>
    /// David, 08/23/03, 1.0.1333. Inherit operation exception </para>
    /// </remarks>
    [Serializable()]
    public class OperationInitializeException : OperationException
    {
        private const string _DefMessage = "Operation failed initializing.";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>
        /// Initializes a new instance of this class.
        /// Uses the default message.
        /// </summary>

        public OperationInitializeException() : base( _DefMessage )
        {
        }

        /// <summary>Constructs a new exception using the given message.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <remarks></remarks>
        public OperationInitializeException( string message ) : base( message )
        {
        }

        /// <summary>Constructs a new exception using the given message and inner exception.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Specifies the exception that was trapped for 
        ///   throwing this exception.</param>
        /// <remarks></remarks>
        public OperationInitializeException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected OperationInitializeException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }

    /// <summary>Handles log-on operation exception.</summary>
    /// <remarks>Use this class to handle log-on that might occur when attempting to login. <para>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 07/29/03, 1.0.1305. Created 
    /// David, 08/23/03, 1.0.1333. Inherit operation exception </para></remarks>
    [Serializable()]
    public class LogOnException : OperationException
    {
        private const string _DefMessage = "Failed to logOn.";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>
        /// Initializes a new instance of this class.
        /// Uses the default message.
        /// </summary>

        public LogOnException() : base( _DefMessage )
        {
        }

        /// <summary>Constructs a new exception using the given message.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <remarks></remarks>
        public LogOnException( string message ) : base( message )
        {
        }

        /// <summary>Constructs a new exception using the given message and inner exception.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Specifies the exception that was trapped for 
        ///   throwing this exception.</param>
        /// <remarks></remarks>
        public LogOnException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected LogOnException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }

    /// <summary>   Handles close operation exceptions. </summary>
    /// <remarks>
    /// Use this class to throw exceptions for operations that were not implemented yet.
    /// <para> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 07/29/03, 1.0.1305. Created </para>
    /// </remarks>
    [Serializable()]
    public class NotImplementedException : OperationException
    {
        private const string _DefMessage = "Operation not implemented.";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>
        /// Initializes a new instance of this class.
        /// Uses the default message.
        /// </summary>

        public NotImplementedException() : base( _DefMessage )
        {
        }

        /// <summary>Constructs a new exception using the given message.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <remarks></remarks>
        public NotImplementedException( string message ) : base( message )
        {
        }

        /// <summary>Constructs a new exception using the given message and inner exception.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Specifies the exception that was trapped for 
        ///   throwing this exception.</param>
        /// <remarks></remarks>
        public NotImplementedException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected NotImplementedException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }
}
