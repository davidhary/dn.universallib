using System;

namespace isr.io.UL
{
    /// <summary>Handles Input Output exceptions.</summary>
    /// <remarks> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 09/12/03, 1.0.1350. Created </para></remarks>
    [Serializable()]
    public class IOException : DeviceExceptionBase
    {
        private const string _DefMessage = "Universal device reported an IO exception.";
        private const string _MessageFormat = "{0} The device reported error number {1}:: {2}";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructor with no parameters.</summary>
        public IOException() : base( _DefMessage )
        {
        }

        /// <summary>Constructor specifying the Message to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        public IOException( string message ) : base( message )
        {
        }

        /// <summary>Constructor specifying the Message and InnerException property to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Sets a reference to the InnerException.</param>
        public IOException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor specifying message and data to be set.</summary>
        /// <param name="errorMessage">Specifies the exception message.</param>
        /// <param name="deviceErrorCode">Specifies the error code from the device.</param>
        /// <param name="deviceErrorMessage">Specifies the error message from the device.</param>
        public IOException( string errorMessage, long deviceErrorCode, string deviceErrorMessage ) : base( _MessageFormat, errorMessage, deviceErrorCode, deviceErrorMessage )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected IOException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }

    /// <summary>Handles data conversion exceptions.</summary>
    /// <remarks> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 09/12/03, 1.0.1350. Created </para></remarks>
    [Serializable()]
    public class CastException : DeviceExceptionBase
    {
        private const string _DefMessage = "Universal device failed converting a value to a new type.";
        private const string _MessageFormat = "{0} The device reported error number {1}:: {2}";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructor with no parameters.</summary>
        public CastException() : base( _DefMessage )
        {
        }

        /// <summary>Constructor specifying the Message to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        public CastException( string message ) : base( message )
        {
        }

        /// <summary>Constructor specifying the Message and InnerException property to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Sets a reference to the InnerException.</param>
        public CastException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor specifying message and data to be set.</summary>
        /// <param name="errorMessage">Specifies the exception message.</param>
        /// <param name="deviceErrorCode">Specifies the error code from the device.</param>
        /// <param name="deviceErrorMessage">Specifies the error message from the device.</param>
        public CastException( string errorMessage, long deviceErrorCode, string deviceErrorMessage ) : base( _MessageFormat, errorMessage, deviceErrorCode, deviceErrorMessage )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected CastException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }

    /// <summary>Handles hardware not found exceptions.</summary>
    /// <remarks> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 05/22/04, 1.0.1603. Created </para></remarks>
    [Serializable()]
    public class HardwareNotFoundException : DeviceExceptionBase
    {
        private const string _DefMessage = "Universal device failed to locate the hardware.";
        private const string _MessageFormat = "{0} The device reported error number {1}:: {2}";

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructor with no parameters.</summary>
        public HardwareNotFoundException() : base( _DefMessage )
        {
        }

        /// <summary>Constructor specifying the Message to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        public HardwareNotFoundException( string message ) : base( message )
        {
        }

        /// <summary>Constructor specifying the Message and InnerException property to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Sets a reference to the InnerException.</param>
        public HardwareNotFoundException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor specifying message and data to be set.</summary>
        /// <param name="errorMessage">Specifies the exception message.</param>
        /// <param name="deviceErrorCode">Specifies the error code from the device.</param>
        /// <param name="deviceErrorMessage">Specifies the error message from the device.</param>
        public HardwareNotFoundException( string errorMessage, long deviceErrorCode, string deviceErrorMessage ) : base( _MessageFormat, errorMessage, deviceErrorCode, deviceErrorMessage )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected HardwareNotFoundException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }
}
