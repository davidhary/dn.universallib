using System;

namespace isr.io.UL
{
    /// <summary> A device exception base. </summary>
    [Serializable]
    public abstract class DeviceExceptionBase : ExceptionBase
    {
        private const string _DefMessage = "General Universal Library failure.";
        private const string _MessageFormat = "{0} The device reported error number {1}:: {2}";

        #region " CUSTOM CONSTRUCTORS "

        /// <summary> Constructor specifying message and data to be set. </summary>
        /// <param name="errorMessage">       Specifies the exception message. </param>
        /// <param name="deviceErrorCode">    Specifies the error code from the device. </param>
        /// <param name="deviceErrorMessage"> Specifies the error message from the device. </param>
        protected DeviceExceptionBase( string errorMessage, long deviceErrorCode, string deviceErrorMessage ) : base( string.Format( System.Globalization.CultureInfo.CurrentCulture, _MessageFormat, errorMessage, deviceErrorCode, deviceErrorMessage ) )
        {
            this.DeviceErrorCode = deviceErrorCode;
            this.DeviceErrorMessage = deviceErrorMessage;
        }

        /// <summary> Constructor specifying message format, message, and data to be set. </summary>
        /// <param name="messageFormat">      Specifies the message format. </param>
        /// <param name="errorMessage">       Specifies the exception message. </param>
        /// <param name="deviceErrorCode">    Specifies the error code from the device. </param>
        /// <param name="deviceErrorMessage"> Specifies the error message from the device. </param>
        protected DeviceExceptionBase( string messageFormat, string errorMessage, long deviceErrorCode, string deviceErrorMessage ) : base( string.Format( System.Globalization.CultureInfo.CurrentCulture, messageFormat, errorMessage, deviceErrorCode, deviceErrorMessage ) )
        {
            this.DeviceErrorCode = deviceErrorCode;
            this.DeviceErrorMessage = deviceErrorMessage;
        }

        /// <summary>Constructor with no parameters.</summary>
        protected DeviceExceptionBase() : base( _DefMessage )
        {
        }

        /// <summary>Constructor specifying the Message to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        protected DeviceExceptionBase( string message ) : base( message )
        {
        }

        /// <summary>Constructor specifying the Message and InnerException property to be set.</summary>
        /// <param name="message">Specifies the exception message.</param>
        /// <param name="innerException">Sets a reference to the InnerException.</param>
        protected DeviceExceptionBase( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>Constructor used for deserialization of the exception class.</summary>
        /// <param name="info">Represents the SerializationInfo of the exception.</param>
        /// <param name="context">Represents the context information of the exception.</param>
        protected DeviceExceptionBase( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the device error code. </summary>
        /// <value> The device error code. </value>
        public long DeviceErrorCode { get; private set; }

        /// <summary> Gets a message describing the device error. </summary>
        /// <value> A message describing the device error. </value>
        public string DeviceErrorMessage { get; private set; }

        #endregion


    }
}
