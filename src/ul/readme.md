# About

isr.io.UL is a .Net library supporting the Universal Library data acquisition modules.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.io.UL is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Universal Library Repository].

[Universal Library Repository]: https://bitbucket.org/davidhary/dn.universallib

