﻿using System;

namespace isr.io.UL.ExceptionExtensions
{

    /// <summary>
    /// Exception methods for adding exception data and building a detailed exception message.
    /// </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class Methods
    {

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, DeviceExceptionBase exception )
        {
            if ( exception is object )
            {
                int count = value.Data.Count;
                value.Data.Add( $"{count}-DeviceErrorCode", exception.DeviceErrorCode );
                value.Data.Add( $"{count}-DeviceMessage", exception.DeviceErrorCode );
            }

            return exception is object;
        }

        /// <summary> Adds an exception data to 'Exception'. </summary>
        /// <remarks> David, 2020-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception exception )
        {
            bool affirmative = false;
            affirmative = affirmative || AddExceptionData( exception, exception as DeviceExceptionBase );
            affirmative = affirmative || AddExceptionData( exception, exception as ArgumentOutOfRangeException );
            affirmative = affirmative || AddExceptionData( exception, exception as ArgumentException );
            affirmative = affirmative || AddExceptionData( exception, exception as System.Runtime.InteropServices.ExternalException );
            return affirmative;
        }

        /// <summary> Adds an exception data. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionDataThis( Exception exception )
        {
            return AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        internal static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString( this Exception value, int level )
        {
            return ToFullBlownString( value, level, AddExceptionDataThis );
        }
    }
}