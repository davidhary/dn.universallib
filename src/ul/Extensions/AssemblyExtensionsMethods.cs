using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace isr.io.UL.AssemblyExtensions
{

    /// <summary>   The assembly extensions methods. </summary>
    /// <remarks>   David, 2022-01-12. </remarks>
    public static class AssemblyExtensionsMethods
    {

        #region " PRODUCT PROCESS CAPTION "

        /// <summary> Prefix process name. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        /// <param name="assembly"> Represents an assembly, which is a reusable, versionable, and self-
        ///                         describing building block of a common language runtime application. 
        /// </param>
        /// <returns> A String. </returns>
        public static string PrefixProcessName( this Assembly assembly )
        {
            if ( assembly is not object )
                throw new ArgumentNullException( nameof( assembly ) );

            string productName = assembly.GetCustomAttributes( typeof( AssemblyProductAttribute ) ).OfType<AssemblyProductAttribute>().FirstOrDefault().Product;
            string processName = Process.GetCurrentProcess().ProcessName;
            if ( !productName.StartsWith( processName, StringComparison.OrdinalIgnoreCase ) )
            {
                productName = $"{processName}.{productName}";
            }

            return productName;
        }

        /// <summary> Builds local time caption. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        /// <param name="timeCaptionFormat"> The time caption format. </param>
        /// <param name="kindFormat">        The kind format. </param>
        /// <returns> A String. </returns>
        public static string BuildLocalTimeCaption( string timeCaptionFormat, string kindFormat )
        {
            string result = string.IsNullOrWhiteSpace( timeCaptionFormat ) ? $"{DateTimeOffset.Now}" : string.IsNullOrWhiteSpace( kindFormat ) ? $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}" : $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}{DateTimeOffset.Now.ToString( kindFormat )}";
            return result;
        }

        /// <summary>   Builds product time caption. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        public static string BuildProductTimeCaption( this Assembly assembly, int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return assembly is not object
                ? throw new ArgumentNullException( nameof( assembly ) )
                : $"{assembly.PrefixProcessName()}.r.{assembly.GetName().Version.ToString( versionElements )} {BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="assembly"> Represents an assembly, which is a reusable, versionable, and self-
        ///                         describing building block of a common language runtime application. </param>
        /// <returns>   A String. </returns>
        public static string BuildProductTimeCaption( this Assembly assembly )
        {
            return assembly.BuildProductTimeCaption( 4, string.Empty, string.Empty );
        }

        #endregion

    }
}
