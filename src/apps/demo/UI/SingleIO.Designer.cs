using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.io.UL.Demo
{
    internal partial class SingleIo
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        private ToolTip tipsToolTip;
        private CheckBox _ledCheckBox;

        private CheckBox ledCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ledCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _ledCheckBox = value;
            }
        }

        private ComboBox _channelCheckBoxD;

        private ComboBox channelCheckBoxD
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _channelCheckBoxD;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _channelCheckBoxD = value;
            }
        }

        private ComboBox _channelCheckBoxC;

        private ComboBox channelCheckBoxC
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _channelCheckBoxC;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _channelCheckBoxC = value;
            }
        }

        private ComboBox _channelCheckBoxB;

        private ComboBox channelCheckBoxB
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _channelCheckBoxB;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _channelCheckBoxB = value;
            }
        }

        private ComboBox _channelCheckBoxA;

        private ComboBox channelCheckBoxA
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _channelCheckBoxA;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _channelCheckBoxA = value;
            }
        }

        private TextBox _analogInputErrorTextBox;

        private TextBox analogInputErrorTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogInputErrorTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogInputErrorTextBox = value;
            }
        }

        private TextBox _voltsTextBoxA;

        private TextBox voltsTextBoxA
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _voltsTextBoxA;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _voltsTextBoxA = value;
            }
        }

        private TextBox _voltsTextBoxB;

        private TextBox voltsTextBoxB
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _voltsTextBoxB;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _voltsTextBoxB = value;
            }
        }

        private TextBox _voltsTextBoxC;

        private TextBox voltsTextBoxC
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _voltsTextBoxC;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _voltsTextBoxC = value;
            }
        }

        private TextBox _voltsTextBoxD;

        private TextBox voltsTextBoxD
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _voltsTextBoxD;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _voltsTextBoxD = value;
            }
        }

        private CheckBox _setCheckBox2;

        private CheckBox setCheckBox2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _setCheckBox2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _setCheckBox2 = value;
            }
        }

        private CheckBox _setCheckBox3;

        private CheckBox setCheckBox3
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _setCheckBox3;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _setCheckBox3 = value;
            }
        }

        private CheckBox _triggerCheckBox2;

        private CheckBox triggerCheckBox2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _triggerCheckBox2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _triggerCheckBox2 = value;
            }
        }

        private CheckBox _triggerCheckBox3;

        private CheckBox triggerCheckBox3
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _triggerCheckBox3;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _triggerCheckBox3 = value;
            }
        }

        private CheckBox _readCheckBox2;

        private CheckBox readCheckBox2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _readCheckBox2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _readCheckBox2 = value;
            }
        }

        private CheckBox _readCheckBox3;

        private CheckBox readCheckBox3
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _readCheckBox3;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _readCheckBox3 = value;
            }
        }

        private CheckBox _resetCountCheckBox;

        private CheckBox resetCountCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _resetCountCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _resetCountCheckBox = value;
            }
        }

        private TextBox _countTextBox;

        private TextBox countTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _countTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _countTextBox = value;
            }
        }

        private TextBox _analogOutOneTextBox;

        private TextBox analogOutOneTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutOneTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutOneTextBox = value;
            }
        }

        private TextBox _analogOutErrorTextBox;

        private TextBox analogOutErrorTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutErrorTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutErrorTextBox = value;
            }
        }

        private Timer _actionTimer;

        private Timer actionTimer
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _actionTimer;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_actionTimer != null)
                {
                    _actionTimer.Tick -= ActionTimer_Tick;
                }

                _actionTimer = value;
                if (_actionTimer != null)
                {
                    _actionTimer.Tick += ActionTimer_Tick;
                }
            }
        }

        private TextBox _analogOutZeroTextBox;

        private TextBox analogOutZeroTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutZeroTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutZeroTextBox = value;
            }
        }

        private Label _channelLabel;

        private Label channelLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _channelLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _channelLabel = value;
            }
        }

        private Label _analogInputErrorLabel;

        private Label analogInputErrorLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogInputErrorLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogInputErrorLabel = value;
            }
        }

        private Label _analogInputLabelA;

        private Label analogInputLabelA
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogInputLabelA;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogInputLabelA = value;
            }
        }

        private Label _analogOutputOneLabel;

        private Label analogOutputOneLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutputOneLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutputOneLabel = value;
            }
        }

        private Label _analogOutputErrorLabel;

        private Label analogOutputErrorLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutputErrorLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutputErrorLabel = value;
            }
        }

        private Label _analogOutputZeroLabel;

        private Label analogOutputZeroLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutputZeroLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutputZeroLabel = value;
            }
        }
        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        private CheckBox _openDeviceCheckBox;

        private CheckBox openDeviceCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _openDeviceCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_openDeviceCheckBox != null)
                {
                    _openDeviceCheckBox.CheckedChanged -= OpenDeviceCheckBox_CheckedChanged;
                }

                _openDeviceCheckBox = value;
                if (_openDeviceCheckBox != null)
                {
                    _openDeviceCheckBox.CheckedChanged += OpenDeviceCheckBox_CheckedChanged;
                }
            }
        }

        private GroupBox _counterGroupBox;

        private GroupBox counterGroupBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _counterGroupBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _counterGroupBox = value;
            }
        }

        private GroupBox _analogOutputGroupBox;

        private GroupBox analogOutputGroupBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogOutputGroupBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogOutputGroupBox = value;
            }
        }

        private Label _countCaptionLabel;

        private Label countCaptionLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _countCaptionLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _countCaptionLabel = value;
            }
        }

        private GroupBox _digitalIoGroupBox;

        private GroupBox digitalIoGroupBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _digitalIoGroupBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _digitalIoGroupBox = value;
            }
        }

        private GroupBox _analogInputGroupBox;

        private GroupBox analogInputGroupBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _analogInputGroupBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _analogInputGroupBox = value;
            }
        }

        private TextBox _messagesTextBox;

        private TextBox messagesTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _messagesTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _messagesTextBox = value;
            }
        }

        private ComboBox _rangeCheckBoxD;

        private ComboBox rangeCheckBoxD
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _rangeCheckBoxD;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _rangeCheckBoxD = value;
            }
        }

        private ComboBox _rangeCheckBoxC;

        private ComboBox rangeCheckBoxC
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _rangeCheckBoxC;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _rangeCheckBoxC = value;
            }
        }

        private ComboBox _rangeCheckBoxB;

        private ComboBox rangeCheckBoxB
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _rangeCheckBoxB;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _rangeCheckBoxB = value;
            }
        }

        private ComboBox _rangeCheckBoxA;

        private ComboBox rangeCheckBoxA
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _rangeCheckBoxA;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _rangeCheckBoxA = value;
            }
        }

        private Label _rangeLabel;

        private Label rangeLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _rangeLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _rangeLabel = value;
            }
        }

        private NumericUpDown _portZeroNumericUpDown;

        private NumericUpDown portZeroNumericUpDown
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portZeroNumericUpDown;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portZeroNumericUpDown = value;
            }
        }

        private NumericUpDown _portOneNumericUpDown;

        private NumericUpDown portOneNumericUpDown
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portOneNumericUpDown;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portOneNumericUpDown = value;
            }
        }

        private CheckBox _portOneReadCheckBox;

        private CheckBox portOneReadCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portOneReadCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portOneReadCheckBox = value;
            }
        }

        private CheckBox _portZeroReadCheckBox;

        private CheckBox portZeroReadCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portZeroReadCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portZeroReadCheckBox = value;
            }
        }

        private CheckBox _portOneInputCheckBox;

        private CheckBox portOneInputCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portOneInputCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portOneInputCheckBox = value;
            }
        }

        private CheckBox _portZeroInputCheckBox;

        private CheckBox portZeroInputCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portZeroInputCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portZeroInputCheckBox = value;
            }
        }

        private CheckBox _portOneSetCheckBox;

        private CheckBox portOneSetCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portOneSetCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portOneSetCheckBox = value;
            }
        }

        private CheckBox _portZeroSetCheckBox;

        private CheckBox portZeroSetCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _portZeroSetCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _portZeroSetCheckBox = value;
            }
        }

        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tipsToolTip = new ToolTip(components);
            _ledCheckBox = new CheckBox();
            _rangeCheckBoxD = new ComboBox();
            _rangeCheckBoxC = new ComboBox();
            _rangeCheckBoxB = new ComboBox();
            _rangeCheckBoxA = new ComboBox();
            _channelCheckBoxD = new ComboBox();
            _channelCheckBoxC = new ComboBox();
            _channelCheckBoxB = new ComboBox();
            _channelCheckBoxA = new ComboBox();
            _analogInputErrorTextBox = new TextBox();
            _voltsTextBoxA = new TextBox();
            _voltsTextBoxB = new TextBox();
            _voltsTextBoxC = new TextBox();
            _voltsTextBoxD = new TextBox();
            _setCheckBox2 = new CheckBox();
            _setCheckBox3 = new CheckBox();
            _triggerCheckBox2 = new CheckBox();
            _triggerCheckBox3 = new CheckBox();
            _readCheckBox2 = new CheckBox();
            _readCheckBox3 = new CheckBox();
            _portOneReadCheckBox = new CheckBox();
            _portZeroReadCheckBox = new CheckBox();
            _portOneInputCheckBox = new CheckBox();
            _portZeroInputCheckBox = new CheckBox();
            _resetCountCheckBox = new CheckBox();
            _portOneSetCheckBox = new CheckBox();
            _portZeroSetCheckBox = new CheckBox();
            _countTextBox = new TextBox();
            _analogOutOneTextBox = new TextBox();
            _analogOutErrorTextBox = new TextBox();
            _actionTimer = new Timer(components);
            _actionTimer.Tick += new EventHandler(ActionTimer_Tick);
            _analogOutZeroTextBox = new TextBox();
            _rangeLabel = new Label();
            _channelLabel = new Label();
            _analogInputErrorLabel = new Label();
            _analogInputLabelA = new Label();
            _countCaptionLabel = new Label();
            _analogOutputOneLabel = new Label();
            _analogOutputErrorLabel = new Label();
            _analogOutputZeroLabel = new Label();
            _openDeviceCheckBox = new CheckBox();
            _openDeviceCheckBox.CheckedChanged += new EventHandler(OpenDeviceCheckBox_CheckedChanged);
            _counterGroupBox = new GroupBox();
            _analogOutputGroupBox = new GroupBox();
            _digitalIoGroupBox = new GroupBox();
            _portOneNumericUpDown = new NumericUpDown();
            _portZeroNumericUpDown = new NumericUpDown();
            _analogInputGroupBox = new GroupBox();
            _messagesTextBox = new TextBox();
            _counterGroupBox.SuspendLayout();
            _analogOutputGroupBox.SuspendLayout();
            _digitalIoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_portOneNumericUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_portZeroNumericUpDown).BeginInit();
            _analogInputGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // ledCheckBox
            // 
            _ledCheckBox.BackColor = SystemColors.Control;
            _ledCheckBox.Checked = true;
            _ledCheckBox.CheckState = CheckState.Checked;
            _ledCheckBox.Cursor = Cursors.Default;
            _ledCheckBox.Enabled = false;
            _ledCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _ledCheckBox.ForeColor = SystemColors.ControlText;
            _ledCheckBox.Location = new Point(320, 24);
            _ledCheckBox.Name = "_ledCheckBox";
            _ledCheckBox.RightToLeft = RightToLeft.No;
            _ledCheckBox.Size = new Size(97, 14);
            _ledCheckBox.TabIndex = 41;
            _ledCheckBox.Text = "Status LED";
            // 
            // rangeCheckBoxD
            // 
            _rangeCheckBoxD.BackColor = SystemColors.Window;
            _rangeCheckBoxD.Cursor = Cursors.Default;
            _rangeCheckBoxD.DropDownStyle = ComboBoxStyle.DropDownList;
            _rangeCheckBoxD.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _rangeCheckBoxD.ForeColor = SystemColors.WindowText;
            _rangeCheckBoxD.Items.AddRange(new object[] { "1", "2", "4", "5", "8", "10", "16", "20" });
            _rangeCheckBoxD.Location = new Point(90, 121);
            _rangeCheckBoxD.Name = "_rangeCheckBoxD";
            _rangeCheckBoxD.RightToLeft = RightToLeft.No;
            _rangeCheckBoxD.Size = new Size(55, 22);
            _rangeCheckBoxD.TabIndex = 40;
            // 
            // rangeCheckBoxC
            // 
            _rangeCheckBoxC.BackColor = SystemColors.Window;
            _rangeCheckBoxC.Cursor = Cursors.Default;
            _rangeCheckBoxC.DropDownStyle = ComboBoxStyle.DropDownList;
            _rangeCheckBoxC.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _rangeCheckBoxC.ForeColor = SystemColors.WindowText;
            _rangeCheckBoxC.Items.AddRange(new object[] { "1", "2", "4", "5", "8", "10", "16", "20" });
            _rangeCheckBoxC.Location = new Point(90, 92);
            _rangeCheckBoxC.Name = "_rangeCheckBoxC";
            _rangeCheckBoxC.RightToLeft = RightToLeft.No;
            _rangeCheckBoxC.Size = new Size(55, 22);
            _rangeCheckBoxC.TabIndex = 39;
            // 
            // rangeCheckBoxB
            // 
            _rangeCheckBoxB.BackColor = SystemColors.Window;
            _rangeCheckBoxB.Cursor = Cursors.Default;
            _rangeCheckBoxB.DropDownStyle = ComboBoxStyle.DropDownList;
            _rangeCheckBoxB.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _rangeCheckBoxB.ForeColor = SystemColors.WindowText;
            _rangeCheckBoxB.Items.AddRange(new object[] { "1", "2", "4", "5", "8", "10", "16", "20" });
            _rangeCheckBoxB.Location = new Point(90, 63);
            _rangeCheckBoxB.Name = "_rangeCheckBoxB";
            _rangeCheckBoxB.RightToLeft = RightToLeft.No;
            _rangeCheckBoxB.Size = new Size(55, 22);
            _rangeCheckBoxB.TabIndex = 38;
            // 
            // rangeCheckBoxA
            // 
            _rangeCheckBoxA.BackColor = SystemColors.Window;
            _rangeCheckBoxA.Cursor = Cursors.Default;
            _rangeCheckBoxA.DropDownStyle = ComboBoxStyle.DropDownList;
            _rangeCheckBoxA.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _rangeCheckBoxA.ForeColor = SystemColors.WindowText;
            _rangeCheckBoxA.Items.AddRange(new object[] { "±20.00V", "±10.0.V", "  ±5.00V", "  ±4.00V", "  ±2.50V", "  ±2.00V", "  ±1.25V", "  ±1.00V" });
            _rangeCheckBoxA.Location = new Point(90, 34);
            _rangeCheckBoxA.Name = "_rangeCheckBoxA";
            _rangeCheckBoxA.RightToLeft = RightToLeft.No;
            _rangeCheckBoxA.Size = new Size(55, 22);
            _rangeCheckBoxA.TabIndex = 35;
            // 
            // channelCheckBoxD
            // 
            _channelCheckBoxD.BackColor = SystemColors.Window;
            _channelCheckBoxD.Cursor = Cursors.Default;
            _channelCheckBoxD.DropDownStyle = ComboBoxStyle.DropDownList;
            _channelCheckBoxD.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _channelCheckBoxD.ForeColor = SystemColors.WindowText;
            _channelCheckBoxD.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _channelCheckBoxD.Location = new Point(16, 121);
            _channelCheckBoxD.Name = "_channelCheckBoxD";
            _channelCheckBoxD.RightToLeft = RightToLeft.No;
            _channelCheckBoxD.Size = new Size(58, 22);
            _channelCheckBoxD.TabIndex = 34;
            // 
            // channelCheckBoxC
            // 
            _channelCheckBoxC.BackColor = SystemColors.Window;
            _channelCheckBoxC.Cursor = Cursors.Default;
            _channelCheckBoxC.DropDownStyle = ComboBoxStyle.DropDownList;
            _channelCheckBoxC.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _channelCheckBoxC.ForeColor = SystemColors.WindowText;
            _channelCheckBoxC.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _channelCheckBoxC.Location = new Point(16, 92);
            _channelCheckBoxC.Name = "_channelCheckBoxC";
            _channelCheckBoxC.RightToLeft = RightToLeft.No;
            _channelCheckBoxC.Size = new Size(58, 22);
            _channelCheckBoxC.TabIndex = 33;
            // 
            // channelCheckBoxB
            // 
            _channelCheckBoxB.BackColor = SystemColors.Window;
            _channelCheckBoxB.Cursor = Cursors.Default;
            _channelCheckBoxB.DropDownStyle = ComboBoxStyle.DropDownList;
            _channelCheckBoxB.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _channelCheckBoxB.ForeColor = SystemColors.WindowText;
            _channelCheckBoxB.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _channelCheckBoxB.Location = new Point(16, 63);
            _channelCheckBoxB.Name = "_channelCheckBoxB";
            _channelCheckBoxB.RightToLeft = RightToLeft.No;
            _channelCheckBoxB.Size = new Size(58, 22);
            _channelCheckBoxB.TabIndex = 32;
            // 
            // channelCheckBoxA
            // 
            _channelCheckBoxA.BackColor = SystemColors.Window;
            _channelCheckBoxA.Cursor = Cursors.Default;
            _channelCheckBoxA.DropDownStyle = ComboBoxStyle.DropDownList;
            _channelCheckBoxA.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _channelCheckBoxA.ForeColor = SystemColors.WindowText;
            _channelCheckBoxA.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _channelCheckBoxA.Location = new Point(16, 34);
            _channelCheckBoxA.Name = "_channelCheckBoxA";
            _channelCheckBoxA.RightToLeft = RightToLeft.No;
            _channelCheckBoxA.Size = new Size(58, 22);
            _channelCheckBoxA.TabIndex = 31;
            // 
            // AnalogInputErrorTextBox
            // 
            _analogInputErrorTextBox.AcceptsReturn = true;
            _analogInputErrorTextBox.AutoSize = false;
            _analogInputErrorTextBox.BackColor = SystemColors.InactiveBorder;
            _analogInputErrorTextBox.Cursor = Cursors.IBeam;
            _analogInputErrorTextBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogInputErrorTextBox.ForeColor = SystemColors.WindowText;
            _analogInputErrorTextBox.Location = new Point(286, 239);
            _analogInputErrorTextBox.MaxLength = 0;
            _analogInputErrorTextBox.Name = "_AnalogInputErrorTextBox";
            _analogInputErrorTextBox.RightToLeft = RightToLeft.No;
            _analogInputErrorTextBox.Size = new Size(273, 33);
            _analogInputErrorTextBox.TabIndex = 29;
            _analogInputErrorTextBox.Text = string.Empty;
            // 
            // VoltsTextBoxA
            // 
            _voltsTextBoxA.AcceptsReturn = true;
            _voltsTextBoxA.AutoSize = false;
            _voltsTextBoxA.BackColor = SystemColors.Window;
            _voltsTextBoxA.Cursor = Cursors.IBeam;
            _voltsTextBoxA.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _voltsTextBoxA.ForeColor = SystemColors.WindowText;
            _voltsTextBoxA.Location = new Point(160, 36);
            _voltsTextBoxA.MaxLength = 0;
            _voltsTextBoxA.Name = "_VoltsTextBoxA";
            _voltsTextBoxA.RightToLeft = RightToLeft.No;
            _voltsTextBoxA.Size = new Size(65, 19);
            _voltsTextBoxA.TabIndex = 24;
            _voltsTextBoxA.Text = string.Empty;
            // 
            // VoltsTextBoxB
            // 
            _voltsTextBoxB.AcceptsReturn = true;
            _voltsTextBoxB.AutoSize = false;
            _voltsTextBoxB.BackColor = SystemColors.Window;
            _voltsTextBoxB.Cursor = Cursors.IBeam;
            _voltsTextBoxB.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _voltsTextBoxB.ForeColor = SystemColors.WindowText;
            _voltsTextBoxB.Location = new Point(160, 65);
            _voltsTextBoxB.MaxLength = 0;
            _voltsTextBoxB.Name = "_VoltsTextBoxB";
            _voltsTextBoxB.RightToLeft = RightToLeft.No;
            _voltsTextBoxB.Size = new Size(65, 19);
            _voltsTextBoxB.TabIndex = 23;
            _voltsTextBoxB.Text = string.Empty;
            // 
            // VoltsTextBoxC
            // 
            _voltsTextBoxC.AcceptsReturn = true;
            _voltsTextBoxC.AutoSize = false;
            _voltsTextBoxC.BackColor = SystemColors.Window;
            _voltsTextBoxC.Cursor = Cursors.IBeam;
            _voltsTextBoxC.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _voltsTextBoxC.ForeColor = SystemColors.WindowText;
            _voltsTextBoxC.Location = new Point(160, 94);
            _voltsTextBoxC.MaxLength = 0;
            _voltsTextBoxC.Name = "_VoltsTextBoxC";
            _voltsTextBoxC.RightToLeft = RightToLeft.No;
            _voltsTextBoxC.Size = new Size(65, 19);
            _voltsTextBoxC.TabIndex = 22;
            _voltsTextBoxC.Text = string.Empty;
            // 
            // VoltsTextBoxD
            // 
            _voltsTextBoxD.AcceptsReturn = true;
            _voltsTextBoxD.AutoSize = false;
            _voltsTextBoxD.BackColor = SystemColors.Window;
            _voltsTextBoxD.Cursor = Cursors.IBeam;
            _voltsTextBoxD.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _voltsTextBoxD.ForeColor = SystemColors.WindowText;
            _voltsTextBoxD.Location = new Point(160, 123);
            _voltsTextBoxD.MaxLength = 0;
            _voltsTextBoxD.Name = "_VoltsTextBoxD";
            _voltsTextBoxD.RightToLeft = RightToLeft.No;
            _voltsTextBoxD.Size = new Size(65, 19);
            _voltsTextBoxD.TabIndex = 21;
            _voltsTextBoxD.Text = string.Empty;
            // 
            // setCheckBox2
            // 
            _setCheckBox2.BackColor = SystemColors.Control;
            _setCheckBox2.Cursor = Cursors.Default;
            _setCheckBox2.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _setCheckBox2.ForeColor = SystemColors.ControlText;
            _setCheckBox2.Location = new Point(142, 68);
            _setCheckBox2.Name = "_setCheckBox2";
            _setCheckBox2.RightToLeft = RightToLeft.No;
            _setCheckBox2.Size = new Size(40, 16);
            _setCheckBox2.TabIndex = 20;
            _setCheckBox2.Text = "Set";
            // 
            // setCheckBox3
            // 
            _setCheckBox3.BackColor = SystemColors.Control;
            _setCheckBox3.Cursor = Cursors.Default;
            _setCheckBox3.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _setCheckBox3.ForeColor = SystemColors.ControlText;
            _setCheckBox3.Location = new Point(142, 92);
            _setCheckBox3.Name = "_setCheckBox3";
            _setCheckBox3.RightToLeft = RightToLeft.No;
            _setCheckBox3.Size = new Size(40, 16);
            _setCheckBox3.TabIndex = 19;
            _setCheckBox3.Text = "Set";
            // 
            // triggerCheckBox2
            // 
            _triggerCheckBox2.BackColor = SystemColors.Control;
            _triggerCheckBox2.Cursor = Cursors.Default;
            _triggerCheckBox2.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _triggerCheckBox2.ForeColor = SystemColors.ControlText;
            _triggerCheckBox2.Location = new Point(17, 68);
            _triggerCheckBox2.Name = "_triggerCheckBox2";
            _triggerCheckBox2.RightToLeft = RightToLeft.No;
            _triggerCheckBox2.Size = new Size(121, 16);
            _triggerCheckBox2.TabIndex = 18;
            _triggerCheckBox2.Text = "IO2 Output/(Input)";
            // 
            // triggerCheckBox3
            // 
            _triggerCheckBox3.BackColor = SystemColors.Control;
            _triggerCheckBox3.Cursor = Cursors.Default;
            _triggerCheckBox3.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _triggerCheckBox3.ForeColor = SystemColors.ControlText;
            _triggerCheckBox3.Location = new Point(17, 92);
            _triggerCheckBox3.Name = "_triggerCheckBox3";
            _triggerCheckBox3.RightToLeft = RightToLeft.No;
            _triggerCheckBox3.Size = new Size(121, 16);
            _triggerCheckBox3.TabIndex = 17;
            _triggerCheckBox3.Text = "IO3 Output/(Input)";
            // 
            // readCheckBox2
            // 
            _readCheckBox2.BackColor = SystemColors.Control;
            _readCheckBox2.Cursor = Cursors.Default;
            _readCheckBox2.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _readCheckBox2.ForeColor = SystemColors.ControlText;
            _readCheckBox2.Location = new Point(199, 68);
            _readCheckBox2.Name = "_readCheckBox2";
            _readCheckBox2.RightToLeft = RightToLeft.No;
            _readCheckBox2.Size = new Size(50, 16);
            _readCheckBox2.TabIndex = 16;
            _readCheckBox2.Text = "Read";
            // 
            // readCheckBox3
            // 
            _readCheckBox3.BackColor = SystemColors.Control;
            _readCheckBox3.Cursor = Cursors.Default;
            _readCheckBox3.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _readCheckBox3.ForeColor = SystemColors.ControlText;
            _readCheckBox3.Location = new Point(199, 92);
            _readCheckBox3.Name = "_readCheckBox3";
            _readCheckBox3.RightToLeft = RightToLeft.No;
            _readCheckBox3.Size = new Size(50, 16);
            _readCheckBox3.TabIndex = 15;
            _readCheckBox3.Text = "Read";
            // 
            // portOneReadCheckBox
            // 
            _portOneReadCheckBox.BackColor = SystemColors.Control;
            _portOneReadCheckBox.Cursor = Cursors.Default;
            _portOneReadCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _portOneReadCheckBox.ForeColor = SystemColors.ControlText;
            _portOneReadCheckBox.Location = new Point(167, 44);
            _portOneReadCheckBox.Name = "_portOneReadCheckBox";
            _portOneReadCheckBox.RightToLeft = RightToLeft.No;
            _portOneReadCheckBox.Size = new Size(50, 16);
            _portOneReadCheckBox.TabIndex = 14;
            _portOneReadCheckBox.Text = "Read";
            // 
            // portZeroReadCheckBox
            // 
            _portZeroReadCheckBox.BackColor = SystemColors.Control;
            _portZeroReadCheckBox.Cursor = Cursors.Default;
            _portZeroReadCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _portZeroReadCheckBox.ForeColor = SystemColors.ControlText;
            _portZeroReadCheckBox.Location = new Point(168, 20);
            _portZeroReadCheckBox.Name = "_portZeroReadCheckBox";
            _portZeroReadCheckBox.RightToLeft = RightToLeft.No;
            _portZeroReadCheckBox.Size = new Size(56, 16);
            _portZeroReadCheckBox.TabIndex = 13;
            _portZeroReadCheckBox.Text = "Read";
            // 
            // portOneInputCheckBox
            // 
            _portOneInputCheckBox.BackColor = SystemColors.Control;
            _portOneInputCheckBox.Checked = true;
            _portOneInputCheckBox.CheckState = CheckState.Checked;
            _portOneInputCheckBox.Cursor = Cursors.Default;
            _portOneInputCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _portOneInputCheckBox.ForeColor = SystemColors.ControlText;
            _portOneInputCheckBox.Location = new Point(17, 44);
            _portOneInputCheckBox.Name = "_portOneInputCheckBox";
            _portOneInputCheckBox.RightToLeft = RightToLeft.No;
            _portOneInputCheckBox.Size = new Size(82, 16);
            _portOneInputCheckBox.TabIndex = 12;
            _portOneInputCheckBox.Text = "Port B Input";
            // 
            // portZeroInputCheckBox
            // 
            _portZeroInputCheckBox.BackColor = SystemColors.Control;
            _portZeroInputCheckBox.Checked = true;
            _portZeroInputCheckBox.CheckState = CheckState.Checked;
            _portZeroInputCheckBox.Cursor = Cursors.Default;
            _portZeroInputCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _portZeroInputCheckBox.ForeColor = SystemColors.ControlText;
            _portZeroInputCheckBox.Location = new Point(17, 20);
            _portZeroInputCheckBox.Name = "_portZeroInputCheckBox";
            _portZeroInputCheckBox.RightToLeft = RightToLeft.No;
            _portZeroInputCheckBox.Size = new Size(82, 16);
            _portZeroInputCheckBox.TabIndex = 11;
            _portZeroInputCheckBox.Text = "Port A Input";
            // 
            // resetCountCheckBox
            // 
            _resetCountCheckBox.BackColor = SystemColors.Control;
            _resetCountCheckBox.Cursor = Cursors.Default;
            _resetCountCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _resetCountCheckBox.ForeColor = SystemColors.ControlText;
            _resetCountCheckBox.Location = new Point(16, 47);
            _resetCountCheckBox.Name = "_resetCountCheckBox";
            _resetCountCheckBox.RightToLeft = RightToLeft.No;
            _resetCountCheckBox.Size = new Size(105, 15);
            _resetCountCheckBox.TabIndex = 10;
            _resetCountCheckBox.Text = "Reset Counter";
            // 
            // portOneSetCheckBox
            // 
            _portOneSetCheckBox.BackColor = SystemColors.Control;
            _portOneSetCheckBox.Cursor = Cursors.Default;
            _portOneSetCheckBox.Enabled = false;
            _portOneSetCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _portOneSetCheckBox.ForeColor = SystemColors.ControlText;
            _portOneSetCheckBox.Location = new Point(110, 44);
            _portOneSetCheckBox.Name = "_portOneSetCheckBox";
            _portOneSetCheckBox.RightToLeft = RightToLeft.No;
            _portOneSetCheckBox.Size = new Size(40, 16);
            _portOneSetCheckBox.TabIndex = 9;
            _portOneSetCheckBox.Text = "Set";
            // 
            // portZeroSetCheckBox
            // 
            _portZeroSetCheckBox.BackColor = SystemColors.Control;
            _portZeroSetCheckBox.Cursor = Cursors.Default;
            _portZeroSetCheckBox.Enabled = false;
            _portZeroSetCheckBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _portZeroSetCheckBox.ForeColor = SystemColors.ControlText;
            _portZeroSetCheckBox.Location = new Point(110, 20);
            _portZeroSetCheckBox.Name = "_portZeroSetCheckBox";
            _portZeroSetCheckBox.RightToLeft = RightToLeft.No;
            _portZeroSetCheckBox.Size = new Size(40, 16);
            _portZeroSetCheckBox.TabIndex = 8;
            _portZeroSetCheckBox.Text = "Set";
            // 
            // countTextBox
            // 
            _countTextBox.AcceptsReturn = true;
            _countTextBox.AutoSize = false;
            _countTextBox.BackColor = SystemColors.Window;
            _countTextBox.Cursor = Cursors.IBeam;
            _countTextBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _countTextBox.ForeColor = SystemColors.WindowText;
            _countTextBox.Location = new Point(49, 18);
            _countTextBox.MaxLength = 0;
            _countTextBox.Name = "_countTextBox";
            _countTextBox.RightToLeft = RightToLeft.No;
            _countTextBox.Size = new Size(80, 19);
            _countTextBox.TabIndex = 6;
            _countTextBox.Text = string.Empty;
            // 
            // analogOutOneTextBox
            // 
            _analogOutOneTextBox.AcceptsReturn = true;
            _analogOutOneTextBox.AutoSize = false;
            _analogOutOneTextBox.BackColor = SystemColors.Window;
            _analogOutOneTextBox.Cursor = Cursors.IBeam;
            _analogOutOneTextBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogOutOneTextBox.ForeColor = SystemColors.WindowText;
            _analogOutOneTextBox.Location = new Point(44, 46);
            _analogOutOneTextBox.MaxLength = 0;
            _analogOutOneTextBox.Name = "_analogOutOneTextBox";
            _analogOutOneTextBox.RightToLeft = RightToLeft.No;
            _analogOutOneTextBox.Size = new Size(65, 19);
            _analogOutOneTextBox.TabIndex = 4;
            _analogOutOneTextBox.Text = "0.00";
            // 
            // analogOutErrorTextBox
            // 
            _analogOutErrorTextBox.AcceptsReturn = true;
            _analogOutErrorTextBox.AutoSize = false;
            _analogOutErrorTextBox.BackColor = SystemColors.InactiveBorder;
            _analogOutErrorTextBox.Cursor = Cursors.IBeam;
            _analogOutErrorTextBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogOutErrorTextBox.ForeColor = SystemColors.WindowText;
            _analogOutErrorTextBox.Location = new Point(8, 239);
            _analogOutErrorTextBox.MaxLength = 0;
            _analogOutErrorTextBox.Name = "_analogOutErrorTextBox";
            _analogOutErrorTextBox.RightToLeft = RightToLeft.No;
            _analogOutErrorTextBox.Size = new Size(273, 33);
            _analogOutErrorTextBox.TabIndex = 1;
            _analogOutErrorTextBox.Text = string.Empty;
            // 
            // actionTimer
            // 
            _actionTimer.Interval = 200;
            // 
            // analogOutZeroTextBox
            // 
            _analogOutZeroTextBox.AcceptsReturn = true;
            _analogOutZeroTextBox.AutoSize = false;
            _analogOutZeroTextBox.BackColor = SystemColors.Window;
            _analogOutZeroTextBox.Cursor = Cursors.IBeam;
            _analogOutZeroTextBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogOutZeroTextBox.ForeColor = SystemColors.WindowText;
            _analogOutZeroTextBox.Location = new Point(44, 22);
            _analogOutZeroTextBox.MaxLength = 0;
            _analogOutZeroTextBox.Name = "_analogOutZeroTextBox";
            _analogOutZeroTextBox.RightToLeft = RightToLeft.No;
            _analogOutZeroTextBox.Size = new Size(65, 19);
            _analogOutZeroTextBox.TabIndex = 0;
            _analogOutZeroTextBox.Text = "0.00";
            // 
            // rangeLabel
            // 
            _rangeLabel.BackColor = SystemColors.Control;
            _rangeLabel.Cursor = Cursors.Default;
            _rangeLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _rangeLabel.ForeColor = SystemColors.ControlText;
            _rangeLabel.Location = new Point(90, 18);
            _rangeLabel.Name = "_rangeLabel";
            _rangeLabel.RightToLeft = RightToLeft.No;
            _rangeLabel.Size = new Size(49, 16);
            _rangeLabel.TabIndex = 37;
            _rangeLabel.Text = "Range";
            // 
            // channelLabel
            // 
            _channelLabel.BackColor = SystemColors.Control;
            _channelLabel.Cursor = Cursors.Default;
            _channelLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _channelLabel.ForeColor = SystemColors.ControlText;
            _channelLabel.Location = new Point(16, 18);
            _channelLabel.Name = "_channelLabel";
            _channelLabel.RightToLeft = RightToLeft.No;
            _channelLabel.Size = new Size(73, 16);
            _channelLabel.TabIndex = 36;
            _channelLabel.Text = "Channel";
            // 
            // analogInputErrorLabel
            // 
            _analogInputErrorLabel.BackColor = SystemColors.Control;
            _analogInputErrorLabel.Cursor = Cursors.Default;
            _analogInputErrorLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogInputErrorLabel.ForeColor = SystemColors.ControlText;
            _analogInputErrorLabel.Location = new Point(285, 223);
            _analogInputErrorLabel.Name = "_analogInputErrorLabel";
            _analogInputErrorLabel.RightToLeft = RightToLeft.No;
            _analogInputErrorLabel.Size = new Size(143, 16);
            _analogInputErrorLabel.TabIndex = 30;
            _analogInputErrorLabel.Text = "Analog Input Error Message";
            // 
            // analogInputLabelA
            // 
            _analogInputLabelA.BackColor = SystemColors.Control;
            _analogInputLabelA.Cursor = Cursors.Default;
            _analogInputLabelA.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogInputLabelA.ForeColor = SystemColors.ControlText;
            _analogInputLabelA.Location = new Point(160, 18);
            _analogInputLabelA.Name = "_analogInputLabelA";
            _analogInputLabelA.RightToLeft = RightToLeft.No;
            _analogInputLabelA.Size = new Size(81, 16);
            _analogInputLabelA.TabIndex = 28;
            _analogInputLabelA.Text = "Voltage";
            // 
            // countCaptionLabel
            // 
            _countCaptionLabel.BackColor = SystemColors.Control;
            _countCaptionLabel.Cursor = Cursors.Default;
            _countCaptionLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _countCaptionLabel.ForeColor = SystemColors.ControlText;
            _countCaptionLabel.Location = new Point(9, 19);
            _countCaptionLabel.Name = "_countCaptionLabel";
            _countCaptionLabel.RightToLeft = RightToLeft.No;
            _countCaptionLabel.Size = new Size(36, 16);
            _countCaptionLabel.TabIndex = 7;
            _countCaptionLabel.Text = "Count: ";
            _countCaptionLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // analogOutputOneLabel
            // 
            _analogOutputOneLabel.BackColor = SystemColors.Control;
            _analogOutputOneLabel.Cursor = Cursors.Default;
            _analogOutputOneLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogOutputOneLabel.ForeColor = SystemColors.ControlText;
            _analogOutputOneLabel.Location = new Point(20, 47);
            _analogOutputOneLabel.Name = "_analogOutputOneLabel";
            _analogOutputOneLabel.RightToLeft = RightToLeft.No;
            _analogOutputOneLabel.Size = new Size(19, 16);
            _analogOutputOneLabel.TabIndex = 5;
            _analogOutputOneLabel.Text = "1: ";
            _analogOutputOneLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // analogOutputErrorLabel
            // 
            _analogOutputErrorLabel.BackColor = SystemColors.Control;
            _analogOutputErrorLabel.Cursor = Cursors.Default;
            _analogOutputErrorLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogOutputErrorLabel.ForeColor = SystemColors.ControlText;
            _analogOutputErrorLabel.Location = new Point(8, 223);
            _analogOutputErrorLabel.Name = "_analogOutputErrorLabel";
            _analogOutputErrorLabel.RightToLeft = RightToLeft.No;
            _analogOutputErrorLabel.Size = new Size(130, 16);
            _analogOutputErrorLabel.TabIndex = 3;
            _analogOutputErrorLabel.Text = "Analog Output Error Message";
            // 
            // analogOutputZeroLabel
            // 
            _analogOutputZeroLabel.BackColor = SystemColors.Control;
            _analogOutputZeroLabel.Cursor = Cursors.Default;
            _analogOutputZeroLabel.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _analogOutputZeroLabel.ForeColor = SystemColors.ControlText;
            _analogOutputZeroLabel.Location = new Point(20, 23);
            _analogOutputZeroLabel.Name = "_analogOutputZeroLabel";
            _analogOutputZeroLabel.RightToLeft = RightToLeft.No;
            _analogOutputZeroLabel.Size = new Size(19, 16);
            _analogOutputZeroLabel.TabIndex = 2;
            _analogOutputZeroLabel.Text = "0: ";
            _analogOutputZeroLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // openDeviceCheckBox
            // 
            _openDeviceCheckBox.Appearance = Appearance.Button;
            _openDeviceCheckBox.FlatStyle = FlatStyle.System;
            _openDeviceCheckBox.Location = new Point(435, 12);
            _openDeviceCheckBox.Name = "_openDeviceCheckBox";
            _openDeviceCheckBox.Size = new Size(120, 32);
            _openDeviceCheckBox.TabIndex = 42;
            _openDeviceCheckBox.Text = "&Open Device";
            _openDeviceCheckBox.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // counterGroupBox
            // 
            _counterGroupBox.Controls.Add(_countTextBox);
            _counterGroupBox.Controls.Add(_countCaptionLabel);
            _counterGroupBox.Controls.Add(_resetCountCheckBox);
            _counterGroupBox.Enabled = false;
            _counterGroupBox.Location = new Point(160, 9);
            _counterGroupBox.Name = "_counterGroupBox";
            _counterGroupBox.Size = new Size(144, 70);
            _counterGroupBox.TabIndex = 43;
            _counterGroupBox.TabStop = false;
            _counterGroupBox.Text = "Counter";
            // 
            // analogOutputGroupBox
            // 
            _analogOutputGroupBox.Controls.Add(_analogOutOneTextBox);
            _analogOutputGroupBox.Controls.Add(_analogOutputZeroLabel);
            _analogOutputGroupBox.Controls.Add(_analogOutputOneLabel);
            _analogOutputGroupBox.Controls.Add(_analogOutZeroTextBox);
            _analogOutputGroupBox.Enabled = false;
            _analogOutputGroupBox.Location = new Point(8, 8);
            _analogOutputGroupBox.Name = "_analogOutputGroupBox";
            _analogOutputGroupBox.Size = new Size(144, 72);
            _analogOutputGroupBox.TabIndex = 44;
            _analogOutputGroupBox.TabStop = false;
            _analogOutputGroupBox.Text = "Analog Output Voltages";
            // 
            // digitalIoGroupBox
            // 
            _digitalIoGroupBox.Controls.Add(_portOneNumericUpDown);
            _digitalIoGroupBox.Controls.Add(_portZeroNumericUpDown);
            _digitalIoGroupBox.Controls.Add(_portZeroInputCheckBox);
            _digitalIoGroupBox.Controls.Add(_triggerCheckBox2);
            _digitalIoGroupBox.Controls.Add(_triggerCheckBox3);
            _digitalIoGroupBox.Controls.Add(_portOneInputCheckBox);
            _digitalIoGroupBox.Controls.Add(_portOneSetCheckBox);
            _digitalIoGroupBox.Controls.Add(_portZeroSetCheckBox);
            _digitalIoGroupBox.Controls.Add(_setCheckBox2);
            _digitalIoGroupBox.Controls.Add(_setCheckBox3);
            _digitalIoGroupBox.Controls.Add(_readCheckBox2);
            _digitalIoGroupBox.Controls.Add(_readCheckBox3);
            _digitalIoGroupBox.Controls.Add(_portOneReadCheckBox);
            _digitalIoGroupBox.Controls.Add(_portZeroReadCheckBox);
            _digitalIoGroupBox.Enabled = false;
            _digitalIoGroupBox.Location = new Point(8, 96);
            _digitalIoGroupBox.Name = "_digitalIoGroupBox";
            _digitalIoGroupBox.Size = new Size(296, 120);
            _digitalIoGroupBox.TabIndex = 45;
            _digitalIoGroupBox.TabStop = false;
            _digitalIoGroupBox.Text = "Digital I/O";
            // 
            // portOneNumericUpDown
            // 
            _portOneNumericUpDown.Hexadecimal = true;
            _portOneNumericUpDown.Location = new Point(240, 42);
            _portOneNumericUpDown.Maximum = new decimal(new int[] { 255, 0, 0, 0 });
            _portOneNumericUpDown.Name = "_portOneNumericUpDown";
            _portOneNumericUpDown.Size = new Size(48, 20);
            _portOneNumericUpDown.TabIndex = 22;
            // 
            // portZeroNumericUpDown
            // 
            _portZeroNumericUpDown.Hexadecimal = true;
            _portZeroNumericUpDown.Location = new Point(240, 18);
            _portZeroNumericUpDown.Maximum = new decimal(new int[] { 255, 0, 0, 0 });
            _portZeroNumericUpDown.Name = "_portZeroNumericUpDown";
            _portZeroNumericUpDown.Size = new Size(48, 20);
            _portZeroNumericUpDown.TabIndex = 21;
            // 
            // analogInputGroupBox
            // 
            _analogInputGroupBox.Controls.Add(_voltsTextBoxD);
            _analogInputGroupBox.Controls.Add(_rangeCheckBoxC);
            _analogInputGroupBox.Controls.Add(_analogInputLabelA);
            _analogInputGroupBox.Controls.Add(_rangeLabel);
            _analogInputGroupBox.Controls.Add(_channelLabel);
            _analogInputGroupBox.Controls.Add(_rangeCheckBoxD);
            _analogInputGroupBox.Controls.Add(_rangeCheckBoxB);
            _analogInputGroupBox.Controls.Add(_rangeCheckBoxA);
            _analogInputGroupBox.Controls.Add(_channelCheckBoxD);
            _analogInputGroupBox.Controls.Add(_channelCheckBoxC);
            _analogInputGroupBox.Controls.Add(_channelCheckBoxB);
            _analogInputGroupBox.Controls.Add(_channelCheckBoxA);
            _analogInputGroupBox.Controls.Add(_voltsTextBoxA);
            _analogInputGroupBox.Controls.Add(_voltsTextBoxB);
            _analogInputGroupBox.Controls.Add(_voltsTextBoxC);
            _analogInputGroupBox.Enabled = false;
            _analogInputGroupBox.Location = new Point(311, 55);
            _analogInputGroupBox.Name = "_analogInputGroupBox";
            _analogInputGroupBox.Size = new Size(248, 160);
            _analogInputGroupBox.TabIndex = 46;
            _analogInputGroupBox.TabStop = false;
            _analogInputGroupBox.Text = "Analog Inputs";
            // 
            // messagesTextBox
            // 
            _messagesTextBox.AcceptsReturn = true;
            _messagesTextBox.AutoSize = false;
            _messagesTextBox.BackColor = SystemColors.InactiveBorder;
            _messagesTextBox.Cursor = Cursors.IBeam;
            _messagesTextBox.Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _messagesTextBox.ForeColor = SystemColors.WindowText;
            _messagesTextBox.Location = new Point(8, 280);
            _messagesTextBox.MaxLength = 0;
            _messagesTextBox.Name = "_messagesTextBox";
            _messagesTextBox.RightToLeft = RightToLeft.No;
            _messagesTextBox.Size = new Size(552, 33);
            _messagesTextBox.TabIndex = 47;
            _messagesTextBox.Text = string.Empty;
            // 
            // SingleIo
            // 
            AutoScaleBaseSize = new Size(5, 13);
            BackColor = SystemColors.Control;
            ClientSize = new Size(568, 318);
            Controls.Add(_messagesTextBox);
            Controls.Add(_analogInputErrorTextBox);
            Controls.Add(_analogOutErrorTextBox);
            Controls.Add(_analogInputGroupBox);
            Controls.Add(_digitalIoGroupBox);
            Controls.Add(_analogOutputGroupBox);
            Controls.Add(_counterGroupBox);
            Controls.Add(_openDeviceCheckBox);
            Controls.Add(_ledCheckBox);
            Controls.Add(_analogInputErrorLabel);
            Controls.Add(_analogOutputErrorLabel);
            Cursor = Cursors.Default;
            Font = new Font("Arial", 8.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            Location = new Point(218, 167);
            Name = "SingleIo";
            RightToLeft = RightToLeft.No;
            StartPosition = FormStartPosition.Manual;
            Text = "PMD-1208LS Single I/O Testing Module";
            _counterGroupBox.ResumeLayout(false);
            _analogOutputGroupBox.ResumeLayout(false);
            _digitalIoGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_portOneNumericUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)_portZeroNumericUpDown).EndInit();
            _analogInputGroupBox.ResumeLayout(false);
            ResumeLayout(false);
        }
    }
}
