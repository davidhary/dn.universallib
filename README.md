# Universal Library

Wrapper for [Measurement Computing] [MccDaq Universal Library].

## Contents
* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Repository Owner](#Repository-Owner)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)
* [Testing the USB-TEMP Board] {#USB-Temp}

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [IDE Repository] - IDE Support repo.
* [Univeral Lib] - Univeral Lib repo.
* [Univeral Lib MCC] - Univeral Lib examples repo.

```
git clone git@bitbucket.org:davidhary/vs.ide.git
git clone git@bitbucket.org:davidhary/dn.universallib.mcc.git
git clone git@bitbucket.org:davidhary/dn.universallib.git
```

Clone the repositories into the following relative path(s) (parents of the .git folder):
```
%vslib%\core\ide
%vslib%\io\universallib
%vslib%\io\universallib.mcc
```

where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]
* [Universal Library]

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants
* [John Simmons] - outlaw programmer
* [Stack overflow]

<a name="Open-Source"></a>
## Open source
Open source used by this software is described and licensed at the
following sites:  
[Univeral Lib]

<a name="Closed-software"></a>
## Closed software 
Closed software used by this software are described and licensed on
[Measurement Computing]

<a name="USB-Temp"></a>
## Testing the USB-TEMP Board
 
### INSTACAL
 
The INSTACAL program is provides by the vendor to configure USB-based and other boards. The INSTACAL program can be used to test these boards.
 
### Testing USB-TEMP with INSTACAL:
* Start INSTACAL
* Select the USB-TEMP board.
* Click Test, Analog Test from the Top Menu
* Check No Scale in the Scale frame.
* Click Test.  Channels having a thermistor connected will display the thermistor resistance.
* Hold the thermistor and verify that the resistance displayed for the associated channel goes down.
 
[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[core]: https://www.bitbucket.org/davidhary/vs.core.git
[Universal Lib]: https://www.bitbucket.org/davidhary/vs.universallib.git
[Universal Lib MCC]: https://www.bitbucket.org/davidhary/vs.universallib.mcc.git
[MccDaq Universal Library]: https://www.mccdaq.com/daq-software/universal-library.aspx

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[Measurement Computing]: https://www.mccdaq.com
[Universal Library]: https://www.mccdaq.com/productsearch.aspx?q=universal%20library

